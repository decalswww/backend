/* eslint-disable import/prefer-default-export */
import { IsString, IsArray } from 'class-validator';
import Tag from '../tag/tag.entity';
import { Transform } from 'class-transformer';
import { json } from 'express';

// Transforms string[] to TagDto[]
const transformTags = tags => {
  tags = JSON.parse(tags);
  console.log(tags);
  if (Array.isArray(tags)) {
    return tags.map(tag => ({ name: tag }));
  }
  return tags;
};

export class CreateDesignDto {
  @IsString()
  name: string;

  @IsString()
  description: string;

  @Transform(transformTags, { toClassOnly: true })
  @IsArray()
  tags: Tag[];

  zipFile: any;

  thumbnailFile: any;
}
