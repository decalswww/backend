import { IsOptional, IsNotEmpty, Min, Max, IsIn } from 'class-validator';
import { Transform } from 'class-transformer';

export default class FilterDesignDto {
  @IsOptional()
  @IsNotEmpty()
  search: string;

  @IsOptional()
  @IsNotEmpty()
  tagSearch: string;

  @IsOptional()
  @Transform(parseInt)
  @Min(1)
  @Max(20)
  limit: number;

  @IsOptional()
  @Transform(parseInt)
  @Min(0)
  offset: number;

  @IsOptional()
  @IsIn([
    'created:ASC',
    'updated:ASC',
    'downloads:ASC',
    'created:DESC',
    'updated:DESC',
    'downloads:DESC',
  ])
  sort: string;
}
