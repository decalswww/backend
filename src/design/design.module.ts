import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import TagService from './tag/tag.service';
import DesignService from './design.service';
import DesignController from './design.controller';
import DesignRepository from './design.repository';
import TagRepository from './tag/tag.repository';
import TagController from './tag/tag.controller';

@Module({
  imports: [TypeOrmModule.forFeature([DesignRepository, TagRepository])],
  controllers: [DesignController, TagController],
  providers: [DesignService, TagService],
  exports: [DesignService, TagService],
})
export default class DesignModule {}
