import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from 'src/users/user.entity';
import Tag from './tag/tag.entity';

@Entity()
export default class Design extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column('text')
  description: string;

  @Column({ default: 0 })
  likes: number;

  @Column({ default: 0 })
  downloads: number;

  @ManyToOne(
    () => User,
    user => user.designs,
    { cascade: true, eager: true },
  )
  creator: User;

  @ManyToMany(
    type => Tag,
    tag => tag.designs,
    { cascade: true, eager: true },
  )
  @JoinTable()
  tags: Tag[];

  // @RelationId((design: Design) => design.creator)
  // creatorId: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column()
  // @Exclude()
  thumbnailFile: string;

  @Column()
  @Exclude()
  zipFile: string;
}
