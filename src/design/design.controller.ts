/* eslint-disable class-methods-use-this */
import {
  Controller,
  Post,
  UseGuards,
  UseInterceptors,
  Body,
  ValidationPipe,
  UsePipes,
  UploadedFiles,
  Get,
  Param,
  Res,
  Delete,
  Query,
  Patch,
} from '@nestjs/common';
import { Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import GetUser from 'src/auth/get-user.decorator';
import {
  designUploadMulterOptions,
  designFileField,
} from './file-upload.utils';
import { CreateDesignDto } from './dto/create-design.dto';
import DesignService from './design.service';
import Design from './design.entity';
import FilterDesignDto from './dto/filter-design.dto';
import { User } from '../users/user.entity';

@Controller('designs')
@UsePipes(
  new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
    transform: true,
  }),
)
export default class DesignController {
  constructor(private designService: DesignService) {}

  @Get()
  async getDesigns(@Query() filterDto: FilterDesignDto): Promise<Design[]> {
    return this.designService.getDesigns(filterDto);
  }

  @Get(':designId')
  async getDesign(@Param('designId') designId: number): Promise<Design> {
    return this.designService.getDesign(designId);
  }

  @Get('tag/:tagname')
  async getDesignByTag(@Param('tagname') tagName: string): Promise<Design[]> {
    return this.designService.getDesignByTag(tagName);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FileFieldsInterceptor(designFileField, designUploadMulterOptions),
  )
  postDesign(
    @GetUser() user: User,
    @UploadedFiles() files,
    @Body()
    createDesignDto: CreateDesignDto,
  ) {
    const dtoWithFile = {
      ...createDesignDto,
      zipFile: files.zip[0].filename,
      thumbnailFile: files.thumbnail[0].filename,
    };
    return this.designService.addDesign(user, dtoWithFile);
  }

  @Patch(':designId')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FileFieldsInterceptor(designFileField, designUploadMulterOptions),
  )
  patchDesign(
    @GetUser() user: User,
    @UploadedFiles() files,
    @Param('designId') designId: number,
    @Body()
    createDesignDto: CreateDesignDto,
  ) {
    const dtoWithFile = {
      ...createDesignDto,
      zipFile: files.zip?.[0].filename,
      thumbnailFile: files.thumbnail?.[0].filename,
    };
    Object.keys(dtoWithFile).forEach(
      key => dtoWithFile[key] === undefined && delete dtoWithFile[key],
    );
    return this.designService.patchDesign(designId, user, dtoWithFile);

    // return this.designService.addDesign(user, dtoWithFile);
  }

  @Get(':designId/thumbnail/:cacheBustingHash')
  async serveThumbnail(
    @Param('designId') designId: number,
    @Res() res: Response,
  ): Promise<void> {
    const options = {
      root: 'files',
      maxAge: 1000 * 60 * 5, // 5 minute cache
    };
    const design = await this.designService.getDesign(designId);
    res.sendFile(design.thumbnailFile, options, err => {
      if (err) {
        res.status(404).send();
        // throw new NotFoundException();
      }
    });
  }

  @Get(':designId/download')
  async serveZip(
    @Param('designId') designId: number,
    @Res() res: Response,
  ): Promise<void> {
    const design = await this.designService.getDesign(designId);
    res.download(
      `files/${design.zipFile}`,
      // `${design.name}.zip`,
      `${design.name}_${design.updated.toISOString().slice(0, 10)}.zip`,
      async err => {
        if (err) {
          res.status(404).send();
        } else {
          await this.designService.incrementDownload(design);
        }
      },
    );
  }

  @Delete(':designId')
  @UseGuards(JwtAuthGuard)
  async deleteDesign(
    @GetUser() user: User,
    @Param('designId') designId: number,
  ): Promise<void> {
    // TODO: check authenticated user against design.creator before deleting
    await this.designService.deleteDesign(designId, user);
  }
}
