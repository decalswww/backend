/* eslint-disable class-methods-use-this */
import {
  Logger,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';

import { EntityRepository, Repository } from 'typeorm';
import { User } from 'src/users/user.entity';
import Design from './design.entity';
import { CreateDesignDto } from './dto/create-design.dto';
import FilterDesignDto from './dto/filter-design.dto';

@EntityRepository(Design)
export default class DesignRepository extends Repository<Design> {
  private logger = new Logger('DesignRepository');

  async getDesigns(filterDto: FilterDesignDto): Promise<Design[]> {
    const { search, tagSearch, sort } = filterDto;
    const query = this.createQueryBuilder('design');
    query.leftJoinAndSelect('design.creator', 'creator');
    query.leftJoinAndSelect('design.tags', 'tags2');

    if (search) {
      query.andWhere(
        '(design.name ILIKE :search OR design.description ILIKE :search)',
        { search: `%${search}%` },
      );
    }
    if (tagSearch) {
      query.innerJoin('design.tags', 'tags', 'tags.name = :tagSearch', {
        tagSearch,
      });
    }
    if (sort) {
      const sortParams = sort.split(':');
      query.addOrderBy(
        `design.${sortParams[0]}`,
        sortParams[1] === 'ASC' ? 'ASC' : 'DESC',
      );
    }
    query.addOrderBy('design.id');
    const limit = filterDto.limit || 12;
    query.take(limit);
    query.skip(filterDto.offset * limit || 0);

    try {
      const designs = await query.getMany();
      return designs;
    } catch (error) {
      this.logger.error(
        `Failed to get designs. Filters: ${JSON.stringify(filterDto)}.`,
        error.stack,
      );
      throw new InternalServerErrorException();
    }
  }

  async crateDesign(
    user: User,
    createDesignDto: CreateDesignDto,
  ): Promise<Design> {
    this.logger.verbose(
      `Creating design for ${JSON.stringify(user)}. Data: ${JSON.stringify(
        createDesignDto,
      )}`,
    );
    const design = new Design();
    design.creator = user;
    design.name = createDesignDto.name;
    design.description = createDesignDto.description;
    design.zipFile = createDesignDto.zipFile;
    design.thumbnailFile = createDesignDto.thumbnailFile;
    design.tags = createDesignDto.tags;

    try {
      await design.save();
    } catch (error) {
      this.logger.error(
        `Failed to create design. data: ${JSON.stringify(createDesignDto)}.`,
        error.stack,
      );
      throw new InternalServerErrorException();
    }
    return design;
  }

  async deleteDesign(id: number, user: User): Promise<void> {
    const result = await this.delete({ id, creator: user });
    if (result.affected === 0) {
      throw new NotFoundException(`Design with ID "${id}" not found`);
    }
  }
}
