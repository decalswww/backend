import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import Design from '../design.entity';

@Entity()
export default class Tag extends BaseEntity {
  constructor(name: string) {
    super();
    this.name = name;
  }

  @PrimaryGeneratedColumn()
  @Exclude()
  id: number;

  @Column({ unique: true })
  name: string;

  @ManyToMany(
    type => Design,
    design => design.tags,
  )
  designs: Design[];
}
