import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import TagRepository from './tag.repository';
import Tag from './tag.entity';

@Injectable()
export default class TagService {
  constructor(
    @InjectRepository(TagRepository)
    private tagRepository: TagRepository,
  ) {}

  async getOrCreateTag(tagName: string): Promise<Tag> {
    return this.tagRepository.getOrCreateTag(tagName);
  }

  async getOrCreateTags(tags: string[]): Promise<Tag[]> {
    const tagPromises = tags.map(t => this.getOrCreateTag(t));
    return Promise.all(tagPromises);
  }

  async getTag(tagName: string): Promise<Tag> {
    return this.tagRepository.getTag(tagName);
  }

  async getTags(tagNames: string[]): Promise<Tag[]> {
    return this.tagRepository.getTags(tagNames);
  }

  async getAllTags(): Promise<Tag[]> {
    return this.tagRepository.find();
  }

  async searchTag(search: string): Promise<Tag[]> {
    return this.tagRepository.searchTags(search);
  }
}
