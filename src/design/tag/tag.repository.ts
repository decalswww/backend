import { Repository, EntityRepository } from 'typeorm';
import { Logger } from '@nestjs/common';
import Tag from './tag.entity';

@EntityRepository(Tag)
export default class TagRepository extends Repository<Tag> {
  private logger = new Logger('TagRepository');

  async getOrCreateTag(tagName: string): Promise<Tag> {
    const tag = await this.getTag(tagName);
    return tag || new Tag(tagName).save();
  }

  async getTag(tagName: string): Promise<Tag> {
    return this.findOne({ where: { name: tagName } });
  }

  async getTags(tagNames: string[]): Promise<Tag[]> {
    const uniqueTags = [...new Set(tagNames)];
    const batch = uniqueTags.map(t => this.getOrCreateTag(t));
    return Promise.all(batch);
  }

  async searchTags(search: string): Promise<Tag[]> {
    const query = this.createQueryBuilder('tag');
    query.where('tag.name ILIKE :search', { search: `%${search}%` });
    query.limit(50);
    return query.getMany();
  }
}
