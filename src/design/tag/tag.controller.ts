import { Controller, Get, Param } from '@nestjs/common';
import TagService from './tag.service';
import Tag from './tag.entity';

@Controller('tag')
export default class TagController {
  constructor(private tagService: TagService) {}

  @Get('search/:search')
  searchTag(@Param('search') search: string): Promise<Tag[]> {
    return this.tagService.searchTag(search);
  }
}
