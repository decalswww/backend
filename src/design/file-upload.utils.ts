import { extname } from 'path';
import { BadRequestException } from '@nestjs/common';
import { diskStorage } from 'multer';

const editFileName = (req, file, callback) => {
  // const name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const randomName = Array(16)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${randomName}${fileExtName}`);
};

const imageFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    callback(
      new BadRequestException(
        `Only image files are allowed for field ${file.fieldname}!`,
      ),
      false,
    );
  } else {
    callback(null, true);
  }
};

const zipFileFilter = (req, file, callback) => {
  if (
    file.mimetype !== 'application/zip' &&
    file.mimetype !== 'application/x-zip-compressed'
  ) {
    callback(
      new BadRequestException(
        `Only zip files are allowed for field ${file.fieldname}!`,
      ),
      false,
    );
  } else {
    callback(null, true);
  }
};

const designUploadFilter = (req, file, callback) => {
  if (file.fieldname === 'zip') {
    zipFileFilter(req, file, callback);
  } else if (file.fieldname === 'thumbnail') {
    imageFileFilter(req, file, callback);
  }
};

export const designFileField = [
  { name: 'zip', maxCount: 1 },
  { name: 'thumbnail', maxCount: 1 },
];

export const designUploadMulterOptions = {
  fileFilter: designUploadFilter,
  storage: diskStorage({
    destination: './files',
    filename: editFileName,
  }),
};
