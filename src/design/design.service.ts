/* eslint-disable class-methods-use-this */
import { Injectable, NotFoundException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/user.entity';
import { CreateDesignDto } from './dto/create-design.dto';
import Design from './design.entity';
import DesignRepository from './design.repository';
import FilterDesignDto from './dto/filter-design.dto';
import TagService from './tag/tag.service';

@Injectable()
export default class DesignService {
  private logger = new Logger('DesignService');

  constructor(
    @InjectRepository(DesignRepository)
    private designRepository: DesignRepository,
    private tagService: TagService,
  ) {}

  async addDesign(
    user: User,
    createDesignDto: CreateDesignDto,
  ): Promise<Design> {
    const tagPromises = createDesignDto.tags.map(t =>
      this.tagService.getOrCreateTag(t.name),
    );
    const tags = await Promise.all(tagPromises);
    createDesignDto.tags = tags;
    return this.designRepository.crateDesign(user, createDesignDto);
  }

  async getDesign(id: number): Promise<Design> {
    const design = await this.designRepository.findOne(id);
    if (design) {
      return design;
    }
    throw new NotFoundException();
  }

  async patchDesign(id: number, user: User, patchDto: CreateDesignDto) {
    this.logger.debug(
      `Patching design ${id}-${user.name} with data: ${JSON.stringify(
        patchDto,
      )}`,
    );
    const designToUpdate = await this.designRepository.findOne(id);
    if (!designToUpdate || designToUpdate.creator.id !== user.id) {
      throw new NotFoundException();
    }
    designToUpdate.name = patchDto.name || designToUpdate.name;
    designToUpdate.description =
      patchDto.description || designToUpdate.description;
    designToUpdate.thumbnailFile =
      patchDto.thumbnailFile || designToUpdate.thumbnailFile;
    designToUpdate.zipFile = patchDto.zipFile || designToUpdate.zipFile;
    if (patchDto.tags) {
      designToUpdate.tags = await this.tagService.getOrCreateTags(
        patchDto.tags.map(t => t.name),
      );
    }

    const updatedDesign = await designToUpdate.save();
    return updatedDesign;
    // if (updatedDesign.affected > 0) {
    //   return this.getDesign(id);
    // }
    // throw new NotFoundException();
  }

  async getDesigns(filterDto: FilterDesignDto): Promise<Design[]> {
    return this.designRepository.getDesigns(filterDto);
  }

  async getDesignByTag(tagToFind: string): Promise<Design[]> {
    const query = this.designRepository.createQueryBuilder('design');
    query
      .innerJoin('design.tags', 'tags', 'tags.name = :tagToFind', {
        tagToFind,
      })
      .leftJoinAndSelect('design.tags', 'tags2')
      .orderBy('design.id')
      .take(10);

    // console.log(query.getSql());

    return query.getMany();
  }

  async deleteDesign(id: number, user: User): Promise<void> {
    return this.designRepository.deleteDesign(id, user);
  }

  async incrementDownload(design: Design): Promise<void> {
    await this.designRepository.update(design.id, {
      downloads: design.downloads + 1,
    });
  }
}
