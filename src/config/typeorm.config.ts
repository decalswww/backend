import { TypeOrmModuleOptions } from '@nestjs/typeorm';
// import * as config from 'config';

// interface DatabaseConfig {
//   type: any;
//   host: string;
//   port: number;
//   username: string;
//   password: string;
//   database: string;
//   synchronize: boolean;
// }

// const dbConfig = config.get<DatabaseConfig>('db');
// console.log(dbConfig);

export default {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT, 10),
  username: process.env.DB_USER,
  password: process.env.DB_PW,
  database: process.env.DB_DB,
  entities: [`${__dirname}/../**/*.entity.{js,ts}`],
  synchronize: true,
} as TypeOrmModuleOptions;
