import { Module } from '@nestjs/common';
import DesignModule from 'src/design/design.module';
import UsersModule from 'src/users/users.module';
import AdminController from './admin.controller';
import RandomDataService from './randomData.service';

@Module({
  controllers: [AdminController],
  providers: [RandomDataService],
  imports: [DesignModule, UsersModule],
})
export default class AdminModule {}
