/* eslint-disable class-methods-use-this */
import {
  Controller,
  UseGuards,
  Get,
  Post,
  UsePipes,
  ValidationPipe,
  Param,
} from '@nestjs/common';
import DesignService from 'src/design/design.service';
import { User } from 'src/users/user.entity';
import Design from 'src/design/design.entity';
import AdminGuard from './admin.guard';
import RandomDataService from './randomData.service';
import Tag from 'src/design/tag/tag.entity';

@Controller('admin')
@UseGuards(AdminGuard)
@UsePipes(
  new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
    transform: true,
  }),
)
export default class AdminController {
  constructor(
    private designService: DesignService,
    private randomData: RandomDataService,
  ) {}

  @Post('random/user')
  creatRandomeUser(): Promise<User> {
    return this.randomData.createRandomUser();
  }

  @Post('random/tag')
  createDummyTag(): Promise<Tag> {
    return this.randomData.createRandomTag();
  }

  @Post('random/tag/:amount')
  createDummyTags(@Param('amount') amount: number): Promise<Tag[]> {
    const tagPromises = Array.from(Array(amount), async () =>
      this.createDummyTag(),
    );
    const tags = Promise.all(tagPromises);
    return tags;
  }

  @Post('random/post')
  createDummyPost(): Promise<Design> {
    return this.randomData.createRandomPost();
  }

  @Post('random/post/:amount')
  createDummyPosts(@Param('amount') amount: number): Promise<Design[]> {
    const designsPromises = Array.from(Array(amount), async () =>
      this.createDummyPost(),
    );
    const designs = Promise.all(designsPromises);
    return designs;
  }

  @Get('random/user')
  getRandomUser(): Promise<User> {
    return this.randomData.getRandomUser();
  }
}
