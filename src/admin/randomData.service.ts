/* eslint-disable class-methods-use-this */
import { Injectable } from '@nestjs/common';
import UsersService from 'src/users/users.service';
import { User, AuthProvider, UserRole } from 'src/users/user.entity';
import * as faker from 'faker';
import CreateUserDto from 'src/users/dto/create-user.dto';
import Design from 'src/design/design.entity';
import DesignService from 'src/design/design.service';
import { CreateDesignDto } from 'src/design/dto/create-design.dto';
import Tag from 'src/design/tag/tag.entity';
import TagService from 'src/design/tag/tag.service';

@Injectable()
export default class RandomDataService {
  constructor(
    private usersService: UsersService,
    private designServce: DesignService,
    private tagService: TagService,
  ) {}

  // getRandomPost(): CreateDummyPostDto {}

  async getRandomUser(): Promise<User> {
    return this.usersService.getRandomsUser();
  }

  tagCache: Tag[] = [];

  async getRandomTag(): Promise<Tag> {
    if (this.tagCache.length === 0) {
      this.tagCache = await this.tagService.getAllTags();
    }
    const randomIndex = Math.floor(Math.random() * this.tagCache.length);
    return this.tagCache[randomIndex];
  }

  async createRandomUser(): Promise<User> {
    const createDto: CreateUserDto = {
      name: faker.name.findName(),
      email: faker.internet.email(),
      avatar: faker.image.avatar(),
      authProvider: AuthProvider.FAKER,
      thirdPartyId: faker.random.alphaNumeric(16),
      userRole: UserRole.DUMMY,
    };
    return this.usersService.createUser(createDto);
  }

  async createRandomTag(): Promise<Tag> {
    const tag = this.tagService.getTag(faker.random.word());
    return tag;
  }

  async createRandomPost(): Promise<Design> {
    const randomTags = Array.from(Array(5), async () => this.getRandomTag());
    const tags = await Promise.all(randomTags);

    const createDto: CreateDesignDto = {
      name: faker.commerce.product(),
      description: faker.hacker.phrase(),
      thumbnailFile: faker.random.image(),
      zipFile: '',
      tags,
    };
    const randomUser = await this.getRandomUser();
    return this.designServce.addDesign(randomUser, createDto);
  }
}
