import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { User, UserRole } from 'src/users/user.entity';

@Injectable()
export default class AdminGuard extends JwtAuthGuard implements CanActivate {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const jwtValid = await super.canActivate(context);
    if (!jwtValid) {
      return false;
    }
    const request = context.switchToHttp().getRequest();
    const { user }: { user: User } = request;
    // TODO verify the role against the db. The role could have changed since the jwt was issued..
    return user.role === UserRole.ADMIN || user.role === UserRole.OWNER;
  }
}
