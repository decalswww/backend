import { AuthProvider } from '../../users/user.entity';

export default class LoginDto {
  authProvider: AuthProvider;

  thirdPartyId: string;

  name: string;

  email: string;

  avatar: string;
}
