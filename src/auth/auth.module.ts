import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { config } from 'dotenv';
import UsersModule from 'src/users/users.module';
import AuthService from './auth.service';
import AuthController from './auth.controller';
import DiscordStrategy from './discord.strategy';
import GoogleStrategy from './google.strategy';
import JwtStrategy from './jwt.strategy';

config();

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '3600s' },
    }),
  ],
  providers: [AuthService, JwtStrategy, GoogleStrategy, DiscordStrategy],
  exports: [AuthService],
  controllers: [AuthController],
})
export default class AuthModule {}
