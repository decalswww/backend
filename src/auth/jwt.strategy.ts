/* eslint-disable class-methods-use-this */
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtPayload } from './jwtPayload.interface';
import AuthService from './auth.service';

@Injectable()
export default class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private authServce: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: JwtPayload, done: VerifiedCallback): Promise<void> {
    try {
      // You could add a function to the authService to verify the claims of the token:
      // i.e. does the user still have the roles that are claimed by the token
      // const validClaims = await this.authService.verifyTokenClaims(payload);

      // if (!validClaims)
      //    return done(new UnauthorizedException('invalid token claims'), false);
      const user = await this.authServce.getUser(payload);
      if (user) {
        done(null, user);
      } else {
        throw new UnauthorizedException('unauthorized');
      }
    } catch (err) {
      throw new UnauthorizedException('unauthorized', err.message);
    }
    // return { userId: payload.sub, username: payload.username };
  }
}
