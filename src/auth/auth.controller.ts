/* eslint-disable class-methods-use-this */
import { Controller, Get, UseGuards, Res, Req } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response, Request } from 'express';
import { JwtUser } from './jwt-auth.guard';

@Controller('auth')
export default class AuthController {
  @Get('google')
  @UseGuards(AuthGuard('google'))
  googleLogin(): void {
    // initiates the Google OAuth2 login flow
  }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  googleLoginCallback(@Req() req: Request, @Res() res: Response): void {
    // handles the Google OAuth2 callback
    const { jwt } = req.user as JwtUser;
    this.onAuthSuccess(jwt, res);
  }

  @Get('discord')
  @UseGuards(AuthGuard('discord'))
  discordLogin(): void {
    // initiates the Discord OAuth2 login flow
  }

  @Get('discord/callback')
  @UseGuards(AuthGuard('discord'))
  discordLoginCallback(@Req() req: Request, @Res() res: Response): void {
    // handles the Discord OAuth2 callback
    const { jwt } = req.user as JwtUser;
    this.onAuthSuccess(jwt, res);
  }

  onAuthSuccess(jwt: string, res: Response): void {
    if (jwt) res.redirect(
               `${process.env.FRONTEND_URL}/auth/succes/${jwt}`,
             );
    else res.redirect(`${process.env.FRONTEND_URL}/auth/failure`);
  }
}
