import { AuthProvider } from '../users/user.entity';

export interface JwtPayload {
  thirdPartyId: string;
  provider: AuthProvider;
}
