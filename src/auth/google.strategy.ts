import { PassportStrategy } from '@nestjs/passport';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { Strategy, VerifyCallback, Profile } from 'passport-google-oauth20';

import { Request } from 'express';
import AuthService from './auth.service';
import LoginDto from './dto/login.dto';
import { AuthProvider } from '../users/user.entity';

@Injectable()
export default class GoogleStrategy extends PassportStrategy(
  Strategy,
  'google',
) {
  constructor(private authService: AuthService) {
    super({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_SECRET,
      callbackURL: `${process.env.BACKEND_URL}/auth/google/callback`,
      passReqToCallback: true,
      scope: ['email', 'profile'],
    });
  }

  async validate(
    request: Request,
    accessToken: string,
    refreshToken: string,
    profile: Profile,
    done: VerifyCallback,
  ): Promise<any> {
    try {
      const loginDto = GoogleStrategy.getLoginDto(profile);
      const user = await this.authService.OAuthLogin(loginDto);
      if (user) {
        const jwt = await this.authService.getJwtForUser(user);
        done(null, { jwt });
      } else {
        throw new InternalServerErrorException();
      }
    } catch (err) {
      done(err, null);
    }
  }

  static getLoginDto(profile: Profile): LoginDto {
    const loginDto: LoginDto = {
      name: profile.displayName,
      email: profile.emails[0].value,
      avatar: profile.photos[0].value,
      thirdPartyId: profile.id,
      authProvider: AuthProvider.GOOGLE,
    };
    return loginDto;
  }
}
