import { PassportStrategy } from '@nestjs/passport';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { Strategy, Profile } from 'passport-discord';
import { VerifyCallback } from 'passport-oauth2';

import { Request } from 'express';
import AuthService from './auth.service';
import { AuthProvider } from '../users/user.entity';
import LoginDto from './dto/login.dto';


@Injectable()
export default class DiscordStrategy extends PassportStrategy(
  Strategy,
  'discord',
) {
  constructor(private authService: AuthService) {
    super({
      clientID: process.env.DISCORD_CLIENT_ID,
      clientSecret: process.env.DISCORD_SECRET,
      callbackURL: `${process.env.BACKEND_URL}/auth/discord/callback`,
      passReqToCallback: true,
      scope: ['identify', 'email'],
    });
  }

  async validate(
    request: Request,
    accessToken: string,
    refreshToken: string,
    profile: Profile,
    done: VerifyCallback,
  ): Promise<any> {
    try {
      const loginDto = DiscordStrategy.getLoginDto(profile);
      const user = await this.authService.OAuthLogin(loginDto);
      if (user) {
        const jwt = await this.authService.getJwtForUser(user);
        done(null, { jwt });
      }else {
        throw new InternalServerErrorException();
      }
    } catch (err) {
      // console.log(err)
      done(err, null);
    }
  }

  static getLoginDto(profile: Profile) {
    const loginDto: LoginDto = {
      thirdPartyId: profile.id,
      authProvider: AuthProvider.DISCORD,
      email: profile.email,
      name: profile.username,
      avatar: `https://cdn.discordapp.com/avatars/${profile.id}/${profile.avatar}.png`,
    };
    return loginDto;
  }
}
