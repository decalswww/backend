import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import UsersService from 'src/users/users.service';
import { User } from 'src/users/user.entity';
import { JwtPayload } from './jwtPayload.interface';
import LoginDto from './dto/login.dto';

@Injectable()
class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async OAuthLogin(loginDto: LoginDto): Promise<User> {
    return this.usersService.OAauthLogin(loginDto);
  }

  async getJwtForUser(user: User): Promise<string> {
    try {
      // You can add some registration logic here,
      // to register the user using their thirdPartyId (in this case their googleId)
      // let user: IUser = await this.usersService.findOneByThirdPartyId(thirdPartyId, provider);

      // if (!user)
      // user = await this.usersService.registerOAuthUser(thirdPartyId, provider);
      // await this.usersService.addUser(user);

      const payload: JwtPayload = {
        thirdPartyId: user.thirdPartyId,
        provider: user.authProvider,
      };

      const jwt: string = this.jwtService.sign(payload);
      return jwt;
    } catch (err) {
      throw new InternalServerErrorException('validateOAuthLogin', err.message);
    }
  }

  async getUser(jwtPayload: JwtPayload): Promise<User> {
    return this.usersService.getUserByJwt(jwtPayload);
  }
}

export default AuthService;
