import { NestFactory, Reflector } from '@nestjs/core';
import { ClassSerializerInterceptor } from '@nestjs/common';
import { config } from 'dotenv';
import * as helmet from 'helmet';
import AppModule from './app.module';

async function bootstrap() {
  config();
  const app = await NestFactory.create(AppModule);
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  app.use(helmet());
  console.log('Running server with CORS enabled');
  app.enableCors();
  console.log(`Starting server on port ${process.env.BACKEND_PORT}`);
  await app.listen(process.env.BACKEND_PORT);
}
bootstrap();
