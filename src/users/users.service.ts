import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import LoginDto from 'src/auth/dto/login.dto';
import { JwtPayload } from '../auth/jwtPayload.interface';
import { User } from './user.entity';
import UserRepository from './user.repository';
import CreateUserDto from './dto/create-user.dto';

@Injectable()
export default class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async OAauthLogin(loginDto: LoginDto): Promise<User> {
    return this.userRepository.OAuthLogin(loginDto);
  }

  async createUser(createDto: CreateUserDto): Promise<User> {
    return this.userRepository.createNewUser(createDto);
  }

  async getUserByJwt(jwtPayload: JwtPayload): Promise<User> {
    return this.userRepository.findOne({
      thirdPartyId: jwtPayload.thirdPartyId,
      authProvider: jwtPayload.provider,
    });
  }

  async getUserAvatar(userId: number): Promise<string> {
    const user = await this.userRepository.findOne(userId);
    if (user) {
      return user.avatar;
    }
    return undefined;
  }

  async countUsers(): Promise<number> {
    return this.userRepository.count();
  }

  cacheUsers: User[] = [];

  async getRandomsUser(): Promise<User> {
    if (this.cacheUsers.length === 0) {
      this.cacheUsers = await this.userRepository.find();
    }
    const randomIndex = Math.floor(Math.random() * this.cacheUsers.length);
    return this.cacheUsers[randomIndex];
  }
}
