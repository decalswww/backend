import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToMany,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import Design from 'src/design/design.entity';

export enum UserRole {
  DUMMY = 'dumdum',
  OWNER = 'owner',
  ADMIN = 'admin',
  EDITOR = 'editor',
}

export enum AuthProvider {
  FAKER = 'faker',
  GOOGLE = 'google',
  DISCORD = 'discord',
}

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Exclude()
  thirdPartyId: string;

  @Column({
    type: 'enum',
    enum: AuthProvider,
  })
  @Exclude()
  authProvider: AuthProvider;

  @Column()
  name: string;

  @Column()
  @Exclude()
  email: string;

  @Column()
  avatar: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.EDITOR,
  })
  // @Exclude()
  role: UserRole;

  @OneToMany(
    () => Design,
    design => design.creator,
    { eager: false },
  )
  @Exclude()
  designs: Design[];

  @CreateDateColumn()
  @Exclude()
  createdDate: Date;

  @UpdateDateColumn()
  @Exclude()
  updateDate: Date;
}
