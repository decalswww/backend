import {
  Logger,
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import LoginDto from 'src/auth/dto/login.dto';
import { User, UserRole } from './user.entity';
import CreateUserDto from './dto/create-user.dto';

const PG_UNIQUE_CONSTRAINT_VIOLATION = '23505';

@EntityRepository(User)
export default class UserRepository extends Repository<User> {
  private logger = new Logger('UserRepository');

  async OAuthLogin(loginDto: LoginDto): Promise<User> {
    const { email, authProvider, thirdPartyId, avatar, name } = loginDto;
    const oldUser = await this.findOne({
      email,
    });
    if (oldUser) {
      oldUser.authProvider = authProvider;
      oldUser.thirdPartyId = thirdPartyId;
      oldUser.name = name;
      oldUser.avatar = avatar;
      await oldUser.save();
      return oldUser;
    }
    this.logger.verbose(`Adding a new OAuth user: ${JSON.stringify(loginDto)}`);
    const createDto: CreateUserDto = { userRole: UserRole.EDITOR, ...loginDto };
    return this.createNewUser(createDto);
  }

  async createNewUser(createUserDto: CreateUserDto): Promise<User> {
    const newUser = new User();
    newUser.email = createUserDto.email;
    newUser.authProvider = createUserDto.authProvider;
    newUser.thirdPartyId = createUserDto.thirdPartyId;
    newUser.name = createUserDto.name;
    newUser.avatar = createUserDto.avatar;
    try {
      await newUser.save();
    } catch (error) {
      if (error.code === PG_UNIQUE_CONSTRAINT_VIOLATION) {
        throw new ConflictException('Username already exists');
      } else {
        this.logger.error(
          `Failed to create new user. Data: ${JSON.stringify(createUserDto)}.`,
          error.stack,
        );
        throw new InternalServerErrorException();
      }
    }
    return newUser;
  }
}
