import { AuthProvider, UserRole } from 'src/users/user.entity';
import { IsString, IsEmail, IsUrl, IsNumber } from 'class-validator';

export default class CreateUserDto {
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsUrl()
  avatar: string;

  @IsString()
  authProvider: AuthProvider;

  @IsString()
  thirdPartyId: string;

  @IsString()
  userRole: UserRole;
}
