/* eslint-disable class-methods-use-this */
import {
  Controller,
  Get,
  UseGuards,
  Logger,
  InternalServerErrorException,
  Param,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import GetUser from 'src/auth/get-user.decorator';
import UsersService from './users.service';
import { User } from './user.entity';

@Controller('user')
export default class UsersController {
  private logger = new Logger('UsersController');

  constructor(private userService: UsersService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  async getUser(@GetUser() user: User): Promise<User> {
    if (user) {
      return user;
    }
    this.logger.error(`Somehow the JwtAuthGuard didn't throw?!`);
    throw new InternalServerErrorException();
  }

  @Get(':userId/avatar')
  async getAvatar(@Param('userId') userId: number): Promise<string> {
    return this.userService.getUserAvatar(userId);
  }
}
