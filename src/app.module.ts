import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import AdminModule from './admin/admin.module';
import DesignModule from './design/design.module';
import UsersModule from './users/users.module';
import AuthModule from './auth/auth.module';
import typeOrmConfig from './config/typeorm.config';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    MulterModule.register({
      dest: './files',
    }),
    TypeOrmModule.forRoot(typeOrmConfig),
    DesignModule,
    AdminModule,
  ],
})
export default class AppModule {}
